'use strict'

var validateType = {
  string: function(value) {
    return typeof(value)==="string" && value.length>0;
  },
  date: function(value) {
    return value instanceof Date;
  }
};

var parseType = {
  string: value => value,
  date: value => new Date(value)
}

function validate(schema, model) {
  let errors = {};
  Object.keys(schema).forEach( key => {
    var field = schema[key];
    
    if(field.required && !model[key]) {
      errors[key] = "required";
      return;
    }
    
    if(model[key] && !validateType[field.type](model[key])) {
      errors[key] = "type expected: " + field.type;
    }
  });

  if(Object.keys(errors).length === 0) {
    return true;
  } else {
    return errors;
  }
}

function parseData(schema, data) {
  Object.keys(schema).forEach( field => {
    if (data[field] !== undefined) {
      var type = schema[field].type;
      this[field] = parseType[type](data[field]);
    }
  });
}

module.exports = {
  validate: validate,
  parseData: parseData
}