'use strict'

var config = require('../lib/config');
var fs = require('fs');
var modelBase = require('./base');

var schema = {
  id: {
    required: true,
    type: "string"
  },
  email: {
    required: true,
    type: "string"
  },
  name: {
    required: true,
    type: "string"
  },
  surname: {
    required: false,
    type: "string"
  },
  created: {
    required: true,
    type: "date"
  },
  updated: {
    required: false,
    type: "date"
  }
};

function User(data) {
  modelBase.parseData.call(this, schema, data);

  var userStorage = config.appPath + config.storage.users;

  this.save = function() {
    if (this.id) {
      return this.update();
    }

    this.id = new Date().getTime().toString();
    this.created = new Date();

    return new Promise( (resolve, reject) => {
      let validate = modelBase.validate(schema, this);
      if(validate !== true) {
        return reject(validate);
      }
      fs.readFile(userStorage, 'utf8', (err, data) => {
        if (err) {
          return reject(err);
        }
        
        var users = JSON.parse(data);
        var newUser = {};
        Object.keys(schema).forEach( key => {
          newUser[key] = this[key];
        })
        users.push(newUser);

        fs.writeFile(userStorage, JSON.stringify(users), (err, data) => {
          resolve(newUser);
        })
      });
    })
  }

  this.delete = function() {
    return new Promise( (resolve, reject) => {
      fs.readFile(userStorage, 'utf8', (err, data) => {
        if (err) {
          return reject(err);
        }
        var users = JSON.parse(data);
        var userIndex = users.findIndex( user => user.id === this.id);
      
        if(userIndex === -1) {
          return reject("User doesn't exist");
        }

        var userDeleted = users.splice(userIndex, 1);

        fs.writeFile(userStorage, JSON.stringify(users), (err, data) => {
          if (err) {
            return reject(err);
          }
          resolve(userDeleted);
        })
      });
    }); 
  }

  this.update = function(userData) {
    return User.update(this.id, userData);
  }
}

User.getAll = function() {
  var userStorage = config.appPath + config.storage.users;

  return new Promise(function(resolve, reject) {
    fs.readFile(userStorage, 'utf8', function (err, data) {
      if (err) {
        return reject(err);
      }

      var users = JSON.parse(data);
      resolve(users.map( userData => new User(userData)));
    })
  });
}

User.findById = function(userId) {
  var userStorage = config.appPath + config.storage.users;

  return new Promise(function(resolve, reject) {
    fs.readFile(userStorage, 'utf8', function (err, data) {
      if (err) {
        return reject(err);
      }

      var users = JSON.parse(data);

      var userFound = users.find( user => user.id===userId);

      if(userFound) {
        resolve(new User(userFound));
      } else {
        reject("User not exist");
      }
    });
  })
}

User.update = function(userId, userData) {
  return new Promise( (resolve, reject) => {
    var userStorage = config.appPath + config.storage.users;
    fs.readFile(userStorage, 'utf8', (err, data) => {
      if (err) {
        return reject(err);
      }

      var users = JSON.parse(data);
      var userFoundIndex = users.findIndex( user => user.id===userId);

      if(userFoundIndex === -1) {
        return reject("User doesn't exist");
      }

      var userUpdated = new User(userData);
      userUpdated.id = userId;
      userUpdated.created = new Date(users[userFoundIndex].created);
      userUpdated.updated = new Date();
      let validate = modelBase.validate(schema, userUpdated);
      if(validate !== true) {
        return reject(validate);
      }
      
      users[userFoundIndex] = userUpdated;

      fs.writeFile(userStorage, JSON.stringify(users), (err, data) => {
        resolve(userUpdated);
      })
    });
  });
}

module.exports = User;