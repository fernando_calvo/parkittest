var express = require('express');
var router = express.Router();
var User = require('../models/users');

/* GET users listing. */
router.get('/', function(req, res) {
  User.getAll()
  .then( users => {
    res.status(200).json(users);
  })
  .catch( error => {
    res.status(500).json(error);
  })
});

/**
 * GET /users/:id
 */
router.get('/:userId', function(req, res) {
  User.findById(req.params.userId)
  .then( user => {
    res.status(200).json(user);
  })
  .catch( error => {
    res.status(404).json(error);
  })
})

/**
 * POST /users
 */
router.post('/', function(req, res) {
  var userData = {
    email: req.body.email,
    name: req.body.name,
    surname: req.body.surname
  };

  var user = new User(userData);
  user.save()
  .then( user => {
    res.status(200).json(user);
  })
  .catch( error => {
    res.status(500).json(error)
  })
})

router.delete('/:userId', function(req, res) {
  var user = User.findById(req.params.userId)
  .then( user => {
    return user.delete();
  })
  .then( user => {
    res.status(200).json(user);
  })
  .catch( error => {
    res.status(500).json(error);
  })
})

router.put('/:userId', function(req, res) {
  var user = User.update(req.params.userId, req.body)
  .then( user => {
    res.status(200).send(user);
  })
  .catch( error => {
    res.status(500).json(error);
  })
})

module.exports = router;
