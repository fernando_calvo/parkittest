'use strict'
var config = require('../config.json');
module.exports = {
  init: function() {
    Object.keys(config).forEach( key => {
      this[key] = config[key];
    });

    if(!this.appPath) {
      this.appPath = __dirname + '/..';
    }
  }
}