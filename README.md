# ParkIt test

This is a basic API to storage users.

# Technology

The API is developed with Node and Express. The users are stored in a simple file in JSON format.

# Endpoints

| Method | url | Description |
| ------ | ------ | ------ |
| GET | / | Welcome message |
| GET | /users | Return an array with all the users |
| GET | /users/:userId | Return a user |
| POST | /users | Saves a user, data required {name, email} |
| DELETE | /users/:userId | Deletes a user |
| PUT | /users/:userId | Updates a user |

# Installation

  - Clone the project in your local
  - Run `npm install`
  - Run `npm start`

You can connect to your server through [localhost:3000]
  
[localhost:3000]: <//localhost:3000>
